import random

Pscore=0
AIscore=0
loseCombos = ["PR", "RS", "SP"]

print('R = rock, P = paper, S = scissors\n')

while True:
    move = input('Your move: ')
    rps = random.choice(["R", "P", "S"])

    print("AI's choice: " + rps)

    if rps + move in loseCombos:
        AIscore += 1

    elif rps==move:
        pass

    else:
        Pscore += 1

    print('\nAI: ' + str(AIscore) + ', Player: ' + str(Pscore) + '\n')

    if AIscore==3:
        print("\nAI Wins")
        break

    elif Pscore==3:
        print("\nYou Win!")
        break
